type InputProps = {
    label?: string;
    type?: "text" | "password" | "email" | "number" | "tel" | "url";
    id?: string;
    value?: string;
    onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    children?: React.ReactNode;
}
const Input = ({
    label = "",
    type = "text",
    id = "myFormInput",
    value = "",
    onChange = () => {},
    children = null
}: InputProps) => {
    return (
        <div 
            className="flex flex-col">

            <label 
                className="uppercase text-sm" 
                htmlFor={id}>
                {label}
            </label>

            <div
                className="min-w-[400px] border border-gray-300 rounded-md px-4 py-2 focus-within:ring-2 focus-within:ring-green-500 focus-within:border-transparent flex flex-row items-center space-x-2">

                <input 
                    type={type} 
                    id={id} 
                    onChange={onChange} 
                    value={value}
                    className="border-transparent focus:border-transparent focus:ring-0 outline-none flex-grow"
                    />

                {children}

            </div>

        </div>
    )
}

export default Input;