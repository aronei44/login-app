type ButtonProps = {
    children?: React.ReactNode;
    label?: string;
    onClick?: () => void;
    type?: "button" | "submit" | "reset";
};
const Button = ({
    children = null,
    label = "",
    onClick = () => {},
    type = "button"
}: ButtonProps) => {
    return (
        <button 
            className="bg-black text-white px-4 py-2 rounded-md shadow-md hover:bg-dark-hover focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-opacity-50"
            onClick={onClick} 
            type={type}>
            {label}
            {children}
        </button>
    )
};

export default Button;