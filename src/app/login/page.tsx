'use client';

import Button from "@/components/form/button";
import Input from "@/components/form/input";
import { useState } from "react";
import { FaEye, FaEyeSlash } from "react-icons/fa";

const Page = () => {
    const [form, setForm] = useState<{
        username: string;
        password: string;
    }>({ username: "", password: "" });
    const [showPassword, setShowPassword] = useState<boolean>(false);
    return (
        <div className="w-full min-h-screen flex justify-center items-center">
            <div className="py-8 px-16 rounded-md shadow-md bg-white w-fit h-fit flex flex-col space-y-8">
                <p className="text-4xl">ACCOUNT LOGIN</p>
                <Input 
                    id="username"
                    label="Username"
                    value={form.username}
                    onChange={(e) => setForm({ ...form, username: e.target.value })}
                    />
                <Input
                    id="password"
                    label="Password"
                    type={showPassword ? "text" : "password"}
                    value={form.password}
                    onChange={(e) => setForm({ ...form, password: e.target.value })}
                    >
                        {showPassword ? (
                            <FaEyeSlash 
                                className="text-gray-400 cursor-pointer"
                                onClick={() => setShowPassword(false)}
                                />
                        ) : (
                            <FaEye 
                                className="text-gray-400 cursor-pointer"
                                onClick={() => setShowPassword(true)}
                                />
                        )}
                </Input>

                <div className="flex flex-row text-sm justify-between">
                    <div>
                        <input type="checkbox" id="rmb-me" />
                        <label htmlFor="rmb-me">Remember me</label>
                    </div>
                    <p>Forgot Password?</p>
                </div>

                <Button 
                    label="Login"
                    onClick={() => console.log(form)}
                />
            </div>
        </div>
    )
}

export default Page;